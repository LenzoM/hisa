/*--------------------------------*- C++ -*----------------------------------*\
|                                                                             |
|    HiSA: High Speed Aerodynamic solver                                      |
|    Copyright (C) 2014-2017 Johan Heyns - CSIR, South Africa                 |
|    Copyright (C) 2014-2017 Oliver Oxtoby - CSIR, South Africa               |
|                                                                             |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       volVectorField;
    object      U;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

dimensions      [0 1 -1 0 0 0 0];

internalField   uniform (3 0 0);

boundaryField
{
    inlet
    {
        type            fixedValue;
        value           uniform (3 0 0);
    }

    outlet
    {
        type            inletOutlet;
        inletValue      uniform (3 0 0);
        value           uniform (3 0 0);
    }

    bottom
    {
        type            symmetryPlane;
    }

    top
    {
        type            symmetryPlane;
    }

    obstacle
    {
        type            slip;
    }

    defaultFaces
    {
        type            empty;
    }
}

// ************************************************************************* //
